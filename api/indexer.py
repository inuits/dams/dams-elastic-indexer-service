import app
import json
import os
import requests

from flask import request
from flask_restful import abort, Resource
from opensearchpy import OpenSearch, helpers
from opensearchpy.helpers.errors import BulkIndexError
from urllib.parse import urlparse


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class ElasticIndexer(metaclass=Singleton):
    def __init__(self):
        self.collection_api_url = os.getenv("COLLECTION_API_URL")
        self.elastic_url = os.getenv("ELASTIC_URL")
        self.es = OpenSearch(self.elastic_url)
        self.headers = {"Authorization": f'Bearer {os.getenv("STATIC_JWT")}'}

    def __create_es_object_from_entity(self, entities_url, entities):
        for entity in entities:
            _id = entity.get("_key", entity["_id"])
            has_mediafile = (
                "primary_mediafile_location" in entity
                or "primary_thumbnail_location" in entity
            )
            entity_metadata = entity.get("metadata", [])
            for metadata in entity_metadata:
                if metadata.get("type") != "components":
                    continue
                for component_metadata in self.__get_metadata_from_component(
                    entities_url, metadata["key"]
                ):
                    if component_metadata not in entity_metadata:
                        entity_metadata.append(component_metadata)
            es_object = {
                "identifiers": entity["identifiers"],
                "metadata": entity_metadata,
                "relations": self.__get_entity_relations(entities_url, _id),
                "type": entity["type"],
                "has_mediafile": has_mediafile,
            }
            yield {"_op_type": "index", "_index": "entities", "_id": _id, "_source": es_object}

    def __create_index(self):
        index_mapping = json.load(open("mapping.json", "r"))
        try:
            self.es.indices.create(index="entities", body=index_mapping)
        except Exception as ex:
            app.logger.error(f"Could not create index: {ex}")

    def __get_entities(self, entities_url, url=None):
        if not url:
            url = entities_url
        entities = self.__send_get_request(url)
        if "results" not in entities:
            return [entities]
        if len(entities["results"]):
            return entities["results"]
        raise Exception("No entities were returned")

    def __get_entity_relations(self, entities_url, entity_id):
        base_url = urlparse(entities_url)._replace(query="").geturl()
        url = f"{base_url}/relations/all"
        if base_url.endswith("entities"):
            url = f"{base_url}/{entity_id}/relations/all"
        return self.__send_get_request(url)

    def __get_metadata_from_component(self, entities_url, component_key):
        base_url = urlparse(entities_url)._replace(query="").geturl()
        result = requests.get(
            f'{base_url.split("entities")[0]}{component_key}', headers=self.headers
        ).json()
        component_metadata = []
        for metadata in result.get("metadata", []):
            if metadata.get("type") == "parent" or "value" not in metadata:
                continue
            if not isinstance(metadata["value"], str):
                # Prevent errors due to bad imports
                metadata["value"] = "import_error"
            component_metadata.append(metadata)
        return component_metadata

    def __get_related_entities(self, entities_url):
        relation_ids = [x["_id"] for x in self.__get_entities(entities_url)]
        body = {
            "query": {
                "bool": {
                    "must": [
                        {"term": {"type": "asset"}},
                        {
                            "nested": {
                                "path": "relations",
                                "query": {
                                    "bool": {
                                        "must": [
                                            {"terms": {"relations.key": relation_ids}}
                                        ]
                                    }
                                },
                            }
                        },
                    ]
                }
            }
        }
        search_result = self.__send_request(f"{self.elastic_url}/_search", True, body)
        related_ids = [x["_id"] for x in search_result.get("hits", {}).get("hits", [])]
        if not relation_ids:
            return []
        url = f'{entities_url.partition("entities")[0]}entities?ids={",".join(related_ids)}'
        try:
            return self.__get_entities(entities_url, url)
        except Exception:
            return []

    def __send_get_request(self, url):
        return self.__send_request(url)

    def __send_request(self, url, post=False, body=None):
        for i in range(3):
            try:
                if post:
                    response = requests.post(url, headers=self.headers, json=body)
                else:
                    response = requests.get(url, headers=self.headers)
                if response.status_code == 200:
                    break
            except requests.exceptions.ConnectionError:
                continue
        else:
            raise Exception(f"Couldn't get response from {url} after 3 tries")
        return response.json()

    def get_es_health(self):
        if not self.es.ping():
            raise Exception("Failed to connect to ElasticSearch")
        if self.es.cluster.health()["status"] == "red":
            raise Exception("ElasticSearch cluster health is red")
        return "healthy"

    def index_entities(self, entities_url, related=False):
        entities_url = f"{self.collection_api_url}{entities_url}"
        if related:
            entities = self.__get_related_entities(entities_url)
        else:
            entities = self.__get_entities(entities_url)
        try:
            helpers.bulk(
                self.es, self.__create_es_object_from_entity(entities_url, entities)
            )
        except BulkIndexError:
            app.logger.error(f"Index not available, creating it")
            self.__create_index()
            helpers.bulk(
                self.es, self.__create_es_object_from_entity(entities_url, entities)
            )

    def remove_entity_from_index(self, _id):
        self.es.delete(index="entities", id=_id)


class IndexerStart(Resource):
    def __get_request_body(self):
        if not (request_body := request.get_json(silent=True)):
            abort(405, message="Invalid input")
        if "location" not in request_body:
            abort(405, message="Missing 'location' in request body")
        return request_body

    def post(self):
        request_body = self.__get_request_body()
        try:
            ElasticIndexer().index_entities(request_body["location"])
        except Exception as ex:
            return str(ex), 405
        return f'Indexing {request_body["location"]} finished successfully', 201
