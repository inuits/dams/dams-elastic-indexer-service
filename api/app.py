import json
import logging
import os
import secrets

from flask import Flask
from flask_restful import Api
from healthcheck import HealthCheck
from indexer import ElasticIndexer
from rabbitmq_pika_flask import RabbitMQ

if os.getenv("SENTRY_ENABLED", False) in ["True", "true", True]:
    import sentry_sdk
    from sentry_sdk.integrations.flask import FlaskIntegration

    sentry_sdk.init(
        dsn=os.getenv("SENTRY_DSN"),
        integrations=[FlaskIntegration()],
        environment=os.getenv("NOMAD_NAMESPACE"),
    )

app = Flask(__name__)
api = Api(app)
app.secret_key = os.getenv("SECRET_KEY", secrets.token_hex(16))

logging.basicConfig(
    format="%(asctime)s %(process)d,%(threadName)s %(filename)s:%(lineno)d [%(levelname)s] %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    level=logging.INFO,
)
logger = logging.getLogger(__name__)
logging.getLogger("opensearchpy").level = logging.ERROR

rabbit = RabbitMQ()
rabbit.init_app(app, "basic", json.loads, json.dumps)


def elastic_search_available():
    return True, ElasticIndexer().get_es_health()


def rabbit_available():
    return True, rabbit.get_connection().is_open


health = HealthCheck()
if os.getenv("HEALTH_CHECK_EXTERNAL_SERVICES", True) in ["True", "true", True]:
    health.add_check(elastic_search_available)
    health.add_check(rabbit_available)
app.add_url_rule("/health", "healthcheck", view_func=lambda: health.run())

from indexer import IndexerStart
import resources.queues

api.add_resource(IndexerStart, "/indexer/start")

if __name__ == "__main__":
    app.run()
