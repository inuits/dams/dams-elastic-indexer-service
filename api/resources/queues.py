import app

from indexer import ElasticIndexer


def __is_malformed_message(data, fields, check_asset=True):
    if not all(x in data for x in fields):
        app.logger.error(f"Message malformed: missing one of {fields}")
        return True
    if check_asset and data["type"] != "asset":
        return True
    return False


@app.rabbit.queue("dams.entity_changed")
def index_asset(routing_key, body, message_id):
    data = body["data"]
    if __is_malformed_message(data, ["location", "type"]):
        return
    try:
        ElasticIndexer().index_entities(data["location"])
    except Exception as ex:
        app.logger.error(f'Indexing {data["location"]} failed with: {ex}')


@app.rabbit.queue("dams.entity_deleted")
def remove_asset_from_index(routing_key, body, message_id):
    data = body["data"]
    if __is_malformed_message(data, ["_id", "type"]):
        return
    try:
        ElasticIndexer().remove_entity_from_index(data["_id"])
    except Exception as ex:
        app.logger.error(f'Removing {data["_id"]} from index failed with: {ex}')


@app.rabbit.queue("dams.edge_changed")
def index_related_assets(routing_key, body, message_id):
    data = body["data"]
    if __is_malformed_message(data, ["location"], check_asset=False):
        return
    try:
        ElasticIndexer().index_entities(data["location"], related=True)
    except Exception as ex:
        app.logger.error(f'Indexing {data["location"]} failed with: {ex}')
